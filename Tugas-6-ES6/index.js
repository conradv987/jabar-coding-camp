//soal 1
const luas=(p , l) => {
    return p * l 
}

let p = 9
let l = 3
let luas_persegi_panjang = luas(p , l)
console.log('Luas Persegi panjang = '+luas_persegi_panjang)

const keliling=(p2 , l2) => {
    return 2*p2 + 2*l2 
}

let p2 = 9
let l2 = 8
let keliling_persegi_panjang = keliling(p2 , l2)
console.log('Keliling Persegi panjang = '+keliling_persegi_panjang) // jawaban soal 1

console.log('\n')

//soal 2
const newFunction = (firstName, lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: function(){
            console.log(firstName + " " + lastName);
        }
    };
};

//Driver Code 
newFunction("Conrad", "Veryand").fullName(); //jawaban soal 2

console.log('\n')

// soal 3
const newObject = {
    firstName: "Conrad",
    lastName: "Veryand",
    address: "Babakan Selakaso",
    hobby: "Gaming",
}

const {firstName, lastName, address, hobby} = newObject;

// Driver code
console.log(firstName, lastName, address, hobby); //jawaban soal 3

console.log('\n')
        
// soal 4
const west = ["Will", "Chris", "Sam", "Holly,"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = (west + east)

//Driver Code
console.log(combined); // jawaban soal 4

console.log('\n')

// soal 5
const planet = "earth";
const view = "glass";
var before = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}`;
console.log(before); // jawaban soal 5


